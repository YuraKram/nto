s=open("17-243.txt").readlines()
for i in range(len(s)):
    s[i]=int(s[i].strip())
a = 0
d = 0
summ = 10000
for i in s:
    if i % 61 == 0:
        b = [int(j) for j in str(i)]
        a += sum(b)
for j in range(len(s)-1):
    if (s[j] > a and s[j] % 100 != 33 and s[j+1] <= a and s[j+1] % 100 == 33) or (s[j+1] > a and s[j+1] % 100 != 33 and s[j] <= a and  s[j] % 100 == 33):
        d += 1
        if s[j] + s[j+1] < summ:
            summ = s[j] + s[j+1]
print(summ, d)
